//Import thư viện Express
const express = require('express');
const { productTypeMiddleware } = require('../middlewares/productTypeMiddleware');
const { getAllProductType,
    getProductTypeById,
    createProductType,
    updateProductType,
    deleteProductType
} = require('../controllers/productTypeController');

//Tạo router
const productTypeRouter = express.Router();

//Sử dụng middleware
productTypeRouter.use(productTypeMiddleware);

//Get all productType
productTypeRouter.get('/productType', getAllProductType)

//Get productType by Id
productTypeRouter.get('/productType/:productTypeID',getProductTypeById )

//Create a productType
productTypeRouter.post('/productType', createProductType);

//Update productType 
productTypeRouter.put('/productType/:productTypeID', updateProductType);
 
//Delete productType
productTypeRouter.delete('/productType/:productTypeID', deleteProductType);

module.exports = {productTypeRouter };