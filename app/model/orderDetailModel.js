//Khai báo thư viện mongoose
const mongoose = require('mongoose');

//khai báo class Schema;
const Schema = mongoose.Schema;

//Khai báo orderDetailModel
const orderDetailModel = new Schema({
// _id :{ 
//     type: Schema.Types.ObjectId,
//     unique: true
// },
product: {
    type: Schema.Types.ObjectId,
    ref: 'Product'
},
quantity:{
    type: Number,
    default: 0
},
timeCreate: {
    type: Date,
    default: Date.now()
},
timeUpdate: {
    type: Date,
    default: Date.now()
},
},{
    timestamps: true
})
module.exports = mongoose.model('orderDetail', orderDetailModel);