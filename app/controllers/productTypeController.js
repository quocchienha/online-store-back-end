//Import thư viện mongoose
const mongoose = require('mongoose');

//Import productType model
const productTypeModel = require('../model/productTypeModel');

const getAllProductType = (req, res) => {
// B1: Thu thập dữ liệu từ req
// B2: Validate dữ liệu
// B3: Gọi model thực hiện các thao tác nghiệp vụ
productTypeModel.find((error, data) => {
if(error) {
    return res.status(500).json({
        message: error.message
    })
}
return res.status(200).json({
    message: 'Get all ProductType',
    productTypes: data
})
})
}
//Get productType by id
const getProductTypeById = (req, res) => {
// B1: Thu thập dữ liệu từ req
let productTypeID = req.params.productTypeID;
// B2: Validate dữ liệu
if(!mongoose.Types.ObjectId.isValid(productTypeID)){
    return res.status(400).json({
        message: 'ProductType ID is invalid'
    })
}
// B3: Gọi model thực hiện các thao tác nghiệp vụ
productTypeModel.find(productTypeID, (error,data) => {
    if(error){
        return res.status(500).json({
        message:error.message
        })
    }
    return res.status(200).json({
        message:'Get a productType by ID',
        productType: data
    })
})
}
//Create ProductType
const createProductType = (req, res) => {
// B1: Thu thập dữ liệu từ req
let body = req.body;
// B2: Validate dữ liệu
if(!body.name){
    return res.status(400).json({
        message:'ProductType name is required!'
    })
}

// B3: Gọi model thực hiện các thao tác nghiệp vụ
let newProductType = {
    _id: mongoose.Types.ObjectId(),
    name: body.name,
    description: body.description,
}
productTypeModel.create(newProductType, (error, data) => {
    if(error){
        return res.status(500).json({
            message:error.message
            })
    }
    return res.status(201).json({
        message:'Create successfully',
        productType: data
    })
})
}
//Update ProductType
const updateProductType = (req, res) => {
// B1: Thu thập dữ liệu từ req
let productTypeID = req.params.productTypeID;
let body = req.body;

// B2: Validate dữ liệu
if(!mongoose.Types.ObjectId.isValid(productTypeID)){
    return res.status(400).json({
        message: 'ProductType ID is invalid'
    })
}
// B3: Gọi model thực hiện các thao tác nghiệp vụ
let updateProductType = {
    _id: mongoose.Types.ObjectId(),
    name: body.name,
    description: body.description,
}
productTypeModel.findByIdAndUpdate(productTypeID, updateProductType, (error, data) => {
    if(error){
        return res.status(500).json({
            message:error.message
            })
    }
    return res.status(200).json({
        message:'Update successfully',
        productType: data
    })
})
}
//delete productType 
const deleteProductType = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let productTypeID = req.params.productTypeID;
// B2: Validate dữ liệu
if(!mongoose.Types.ObjectId.isValid(productTypeID)){
    return res.status(400).json({
        message: 'ProductType ID is invalid'
    })
}
// B3: Gọi model thực hiện các thao tác nghiệp vụ
productTypeModel.findOneAndDelete(productTypeID, (error, data)=>{
    if(error){
        return res.status(500).json({
            message:error.message
            })
    }
    return res.status(200).json({
        message:'Update successfully',
    })
})
}


module.exports = {
    getAllProductType,
    getProductTypeById,
    createProductType,
    updateProductType,
    deleteProductType
}
