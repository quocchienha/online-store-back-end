//Import thư viện Express
const express = require('express');
const { orderMiddleware } = require('../middlewares/orderMiddleware');
const {  getAllCustomer,
    getAllOders,
    getOrderById,
    createOrder,
    updateOrder,
    deleteOrder
} = require('../controllers/orderController');
//Tạo router
const orderRouter = express.Router();

//Sử dụng middleware
orderRouter.use(orderMiddleware);

//Get all product
orderRouter.get('/order', getAllOders)

//Get product by Id
orderRouter.get('/order/:orderID',getOrderById )

//Create a product
orderRouter.post('/order', createOrder);

//Update product
orderRouter.put('/order/:orderID', updateOrder);
 
//Delete product
orderRouter.delete('/order/:orderID', deleteOrder);

module.exports = { orderRouter };