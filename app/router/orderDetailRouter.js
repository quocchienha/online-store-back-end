//Khai báo thư viện express
const express = require('express')

//import middleware
const {orderDetailMiddleware} = require('../middlewares/orderDetailMiddleware');

//import orderdetailController
const {
    getAllOrderDetail,
    getOrderDetailById,
    createOrderDetail,
    updateOrderDetail,
    deleteOrderDetail
} = require('../controllers/orderDetailController');

//tạo router
const orderDetilRouter = express.Router();

//sử dụng middleware
orderDetilRouter.use(orderDetailMiddleware);

//get all orderDetail
orderDetilRouter.get('/orderDetail' , getAllOrderDetail);

//Get orderDetail by id
orderDetilRouter.get('/orderDetail/:orderDetailID', getOrderDetailById)

//Create a orderDetail
orderDetilRouter.post('/orderDetail', createOrderDetail);

//Update orderDetail
orderDetilRouter.put('/orderDetail/:orderDetailID', updateOrderDetail);
 
//Delete orderDetail
orderDetilRouter.delete('/orderDetail/:orderDetailID', deleteOrderDetail);
module.exports = {orderDetilRouter};