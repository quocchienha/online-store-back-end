//Import thư viện mongoose
const mongoose = require('mongoose');

//khai báo class Schema;
const Schema = mongoose.Schema;

//khai báo ProductType
const orderModel = new Schema({
    // _id :{
    //     type: Schema.Types.ObjectId,
    //     unique: true
    // }, 
    orderDate: {
        type: Date,
        default: Date.now() 
    },
    shippedDate: {
        type: Date,
    },
    note:{
         type: String
    },
    orderDetails: [{
        type: Schema.Types.ObjectId,
        ref: 'OrderDetail'
    }],
    cost: {
        type: Number,
        default: 0
    },
    timeCreated: {
        type: Date,
        default: Date.now() 	
    },
    timeUpdated: {
        type: Date,
        default: Date.now() 	
    }
},{
    timestamps: true
})
module.exports = mongoose.model('orders', orderModel)