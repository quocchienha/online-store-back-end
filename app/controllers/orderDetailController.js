//Khai báo thư viện mongoose
const mongoose = require('mongoose');

//import OrderDetailModel
const orderDetaillModel = require('../model/orderDetailModel');

//Get all orderDetail
const getAllOrderDetail = (req, res) =>{
    // B1: Thu thập dữ liệu từ req
// B2: Validate dữ liệu
// B3: Gọi model thực hiện các thao tác nghiệp vụ
orderDetaillModel.find((error, data)=> {
if(error){
    return res.status(500).json({
        message: error.message
    })
}
return res.status(200).json({
    message: 'Get all OrderDetail',
    orderDetail: data
})
})
};

//Get orderDetail by ID
const getOrderDetailById = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let orderDetailID = req.params.orderDetailID;
// B2: Validate dữ liệu
if (!mongoose.Types.ObjectId.isValid(orderDetailID)){
    return res.status(400).json({
        message: 'OrderDetail Id is invalid'
    })
}
// B3: Gọi model thực hiện các thao tác nghiệp vụ
orderDetaillModel.find(orderDetailID, (req, res) => {
 if(error){
    return res.status(500).json({
        message: error.message
    })
 }
 return res.status(200).json({
    message: 'Get order Detail by id',
    orderDetail : data
 })
})
}
//Create orderDetail 
const createOrderDetail = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let body = req.body;
// B2: Validate dữ liệu
if(!body.product){
    return res.staus(400).json({
        message : 'Create product is invalid'
    })
}
if(!body.quanity){
    return res.staus(400).json({
        message : 'Create quanity is invalid'
    })
}
// B3: Gọi model thực hiện các thao tác nghiệp vụ
let newOrderDetail = {
    _id: mongoose.Types.ObjectId,
    product: body.product,
    quanity: body.quanity
}
orderDetaillModel.create(newOrderDetail, (req, res) => {
    if(error){
        return res.status(500).json({
            message: error.message
        })
    }
    return res.status(201).json({
        message: 'Create order detail successfully',
        orderDetail: data
    })
})
}
//Update orderDetail
const updateOrderDetail = (req,res) =>{
 // B1: Thu thập dữ liệu từ req
 let orderDetailID = req.params.orderDetailID;
 let body = req.body;
// B2: Validate dữ liệu
if(!body.product){
    return res.staus(400).json({
        message : 'Create product is invalid'
    })
}
if(!body.quanity){
    return res.staus(400).json({
        message : 'Create quanity is invalid'
    })
}
// B3: Gọi model thực hiện các thao tác nghiệp vụ
let orderDetailUpdate = {
    id: mongoose.Types.ObjectId,
    product: body.product,
    quanity: body.quanity
}
orderDetaillModel.findByIdAndUpdate(orderDetailID, orderDetailUpdate, (req,res) =>{
if(error){
    return res.status(500).json({
        message: error.message
    })
}
return res.staus(200).json({
    message:'Update successfully',
    orderDetail: data
})
})
}
//Delete orderDetail 
const deleteOrderDetail = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let orderDetailID = req.params.orderDetailID;
// B2: Validate dữ liệu
if(!mongoose.Types.ObjectId.isValid(orderDetailID)){
    return res.status(400).json({
        message: 'orderDetail ID is invalid'
    })
}
// B3: Gọi model thực hiện các thao tác nghiệp vụ
orderDetaillModel.findByIdAndDelete(orderDetailID, (error, data) => {
    if(error){
         return res.status(500).json({
        message: error.message
    })
}
return res.staus(200).json({
    message:'Delete successfully',
})
})
}
module.exports = {
    getAllOrderDetail,
    getOrderDetailById,
    createOrderDetail,
    updateOrderDetail,
    deleteOrderDetail
}