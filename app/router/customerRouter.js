//Import thư viện Express
const express = require('express');
const { customerMiddleware } = require('../middlewares/customerMiddleware');
const {  getAllCustomer,
    getCustomerById,
    createCustomer,
    updateCustomer,
    deleteCustomer
} = require('../controllers/customerController');

//Tạo router
const customerRouter = express.Router();

//Sử dụng middleware
customerRouter.use(customerMiddleware);

//Get all customer
customerRouter.get('/customer', getAllCustomer)

//Get customer by Id
customerRouter.get('/customer/:customerID',getCustomerById )

//Create a customer
customerRouter.post('/customer', createCustomer);

//Update customer
customerRouter.put('/customer/:customerID', updateCustomer);
 
//Delete customer
customerRouter.delete('/customer/:customerID', deleteCustomer);

module.exports = { customerRouter };