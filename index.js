//import thư viện express
const express = require('express');

//import thư viện path
const path = require('path');

//Import thư viện mongoose
const mongoose = require('mongoose');

//connect mongodb
mongoose.connect('mongodb+srv://qch231198:Quocchien98@cluster0.gytldq2.mongodb.net/CRUD_Shop24h', function (error) {
   if(error) throw error;
   console.log('Successfully connected');
});
   
//Improt router modules
const {productTypeRouter } = require('./app/router/productTypeRouter');
const {productRouter} = require('./app/router/productRouter');
const {customerRouter} = require('./app/router/customerRouter');
const {orderRouter} = require('./app/router/orderRouter');
const {orderDetilRouter} = require('./app/router/orderDetailRouter');
//tạp app
const app = express();

//tạo cổng chạy project
const port = 8000;

// Sử dụng body json
app.use(express.json());

//sử dụng unicode
app.use(express.urlencoded({
    extended:true
}))

app.use(productTypeRouter);
app.use(productRouter);
app.use(customerRouter);
app.use(orderRouter);
app.use(orderDetilRouter);


app.listen(port, () => {
    console.log(`App listening to port ${port}`);
})