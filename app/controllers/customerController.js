//Import thư viện mongoose
const mongoose = require('mongoose');

//Import productType model
const customerModel = require('../model/customerModel');

const getAllCustomer = (req, res) => {
// B1: Thu thập dữ liệu từ req
// B2: Validate dữ liệu
// B3: Gọi model thực hiện các thao tác nghiệp vụ
customerModel.find((error, data) => {
if(error) {
    return res.status(500).json({
        message: error.message
    })
}
return res.status(200).json({
    message: 'Get all Customer',
    customer: data
})
})
}
//Get productType by id
const getCustomerById = (req, res) => {
// B1: Thu thập dữ liệu từ req
let customerID = req.params.customerID;
// B2: Validate dữ liệu
if(!mongoose.Types.ObjectId.isValid(customerID)){
    return res.status(400).json({
        message: 'Customer ID is invalid'
    })
}
// B3: Gọi model thực hiện các thao tác nghiệp vụ
customerModel.find(customerID, (error,data) => {
    if(error){
        return res.status(500).json({
        message:error.message
        })
    }
    return res.status(200).json({
        message:'Get a Customer by ID',
        customer: data
    })
})
}
//Create ProductType
const createCustomer = (req, res) => {
// B1: Thu thập dữ liệu từ req
let body = req.body;
// B2: Validate dữ liệu
if(!body.fullName){
    return res.status(400).json({
        message:'Customer fullName is required!'
    })
}
if(!body.phone){
    return res.status(400).json({
        message:'Customer phone is required!'
    })
}
if(!body.email){
    return res.status(400).json({
        message:'Customer email is required!'
    })
}
if(!body.address){
    return res.status(400).json({
        message:'Customer address is required!'
    })
}
if(!body.city){
    return res.status(400).json({
        message:'Customer city is required!'
    })
}
if(!body.country){
    return res.status(400).json({
        message:'Customer country is required!'
    })
}


// B3: Gọi model thực hiện các thao tác nghiệp vụ
let newCustomer = {
    _id: mongoose.Types.ObjectId(),
    fullName: body.fullName,
    phone: body.phone,
    email: body.email,
    address: body.address,
    city: body.city,
    country: body.country,
    orders: body.orders,
}
customerModel.create(newCustomer, (error, data) => {
    if(error){
        return res.status(500).json({
            message:error.message
            })
    }
    return res.status(201).json({
        message:'Create successfully',
        customer: data
    })
})
}
//Update ProductType
const updateCustomer = (req, res) => {
// B1: Thu thập dữ liệu từ req
let customerID = req.params.customerID;
let body = req.body;

// B2: Validate dữ liệu
if(!mongoose.Types.ObjectId.isValid(customerID)){
    return res.status(400).json({
        message: 'Customer ID is invalid'
    })
}
// B3: Gọi model thực hiện các thao tác nghiệp vụ
let customerUpdate = {
    _id: mongoose.Types.ObjectId(),
    fullName: body.fullName,
    phone: body.phone,
    email: body.email,
    address: body.address,
    city: body.city,
    country: body.country,
    orders: body.orders,
}
customerModel.findByIdAndUpdate(customerID, customerUpdate, (error, data) => {
    if(error){
        return res.status(500).json({
            message:error.message
            })
    }
    return res.status(200).json({
        message:'Update successfully',
        customer: data
    })
})
}
//delete productType 
const deleteCustomer = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let customerID = req.params.customerID;
// B2: Validate dữ liệu
if(!mongoose.Types.ObjectId.isValid(customerID)){
    return res.status(400).json({
        message: 'Customer ID is invalid'
    })
}
// B3: Gọi model thực hiện các thao tác nghiệp vụ
customerModel.findOneAndDelete(customerID, (error, data)=>{
    if(error){
        return res.status(500).json({
            message:error.message
            })
    }
    return res.status(200).json({
        message:'Update successfully',
    })
})
}


module.exports = {
    getAllCustomer,
    getCustomerById,
    createCustomer,
    updateCustomer,
    deleteCustomer
}
