//Import thư viện mongoose
const mongoose = require('mongoose');

//Import productType model
const productModel = require('../model/productModel');

const getAllProduct = (req, res) => {
// B1: Thu thập dữ liệu từ req
// B2: Validate dữ liệu
// B3: Gọi model thực hiện các thao tác nghiệp vụ
productModel.find((error, data) => {
if(error) {
    return res.status(500).json({
        message: error.message
    })
}
return res.status(200).json({
    message: 'Get all Product',
    product: data
})
})
}
//Get productType by id
const getProductById = (req, res) => {
// B1: Thu thập dữ liệu từ req
let productID = req.params.productID;
// B2: Validate dữ liệu
if(!mongoose.Types.ObjectId.isValid(productID)){
    return res.status(400).json({
        message: 'ProductType ID is invalid'
    })
}
// B3: Gọi model thực hiện các thao tác nghiệp vụ
productModel.find(productID, (error,data) => {
    if(error){
        return res.status(500).json({
        message:error.message
        })
    }
    return res.status(200).json({
        message:'Get a productType by ID',
        product: data
    })
})
}
//Create ProductType
const createProduct = (req, res) => {
// B1: Thu thập dữ liệu từ req
let body = req.body;
// B2: Validate dữ liệu
if(!body.name){
    return res.status(400).json({
        message:'Product name is required!'
    })
}
if(!body.type){
    return res.status(400).json({
        message:'Product type is required!'
    })
}
if(!body.imageUrl){
    return res.status(400).json({
        message:'Product imageUrl is required!'
    })
}
if(!body.buyPrice){
    return res.status(400).json({
        message:'Product buyPrice is required!'
    })
}
if(!body.buyPrice){
    return res.status(400).json({
        message:'Product buyPrice is required!'
    })
}
if(!body.promotionPrice){
    return res.status(400).json({
        message:'Product promotionPrice is required!'
    })
}
if(!body.amount){
    return res.status(400).json({
        message:'Product amount is required!'
    })
}

// B3: Gọi model thực hiện các thao tác nghiệp vụ
let newProduct = {
    _id: mongoose.Types.ObjectId(),
    name: body.name,
    description: body.description,
    type: body.type,
    imageUrl: body.imageUrl,
    buyPrice: body.buyPrice,
    promotionPrice: body.promotionPrice,
    amount: body.amount,
}
productModel.create(newProduct, (error, data) => {
    if(error){
        return res.status(500).json({
            message:error.message
            })
    }
    return res.status(201).json({
        message:'Create successfully',
        product: data
    })
})
}
//Update ProductType
const updateProduct = (req, res) => {
// B1: Thu thập dữ liệu từ req
let productID = req.params.productID;
let body = req.body;

// B2: Validate dữ liệu
if(!mongoose.Types.ObjectId.isValid(productID)){
    return res.status(400).json({
        message: 'ProductType ID is invalid'
    })
}
// B3: Gọi model thực hiện các thao tác nghiệp vụ
let updateProduct = {
    _id: mongoose.Types.ObjectId(),
    name: body.name,
    description: body.description,
    type: body.type,
    imageUrl: body.imageUrl,
    buyPrice: body.buyPrice,
    promotionPrice: body.promotionPrice,
    amount: body.amount,
}
productModel.findByIdAndUpdate(productID, updateProduct, (error, data) => {
    if(error){
        return res.status(500).json({
            message:error.message
            })
    }
    return res.status(200).json({
        message:'Update successfully',
        product: data
    })
})
}
//delete productType 
const deleteProduct = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let productID = req.params.productID;
// B2: Validate dữ liệu
if(!mongoose.Types.ObjectId.isValid(productID)){
    return res.status(400).json({
        message: 'ProductType ID is invalid'
    })
}
// B3: Gọi model thực hiện các thao tác nghiệp vụ
productModel.findOneAndDelete(productID, (error, data)=>{
    if(error){
        return res.status(500).json({
            message:error.message
            })
    }
    return res.status(200).json({
        message:'delete successfully',
    })
})
}

module.exports = {
    getAllProduct,
    getProductById,
    createProduct,
    updateProduct,
    deleteProduct
}
