//Import thư viện mongoose
const mongoose = require('mongoose');

//khai báo class Schema;
const Schema = mongoose.Schema;

//khai báo ProductType
const productTypeModel = new Schema({
    // _id :{
    //     type: Schema.Types.ObjectId,
    //     unique: true
    // }, 
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type:String
    },
    timeCreated: {
        type: Date,
        default: Date.now() 	
    },
    timeUpdated: {
        type: Date,
        default: Date.now() 	
    }
},{
    timestamps: true
})
module.exports = mongoose.model('productType', productTypeModel)