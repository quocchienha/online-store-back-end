//Import thư viện mongoose
const mongoose = require('mongoose');

//khai báo class Schema;
const Schema = mongoose.Schema;

//khai báo ProductType
const productModel = new Schema({
    // _id :{
    //     type: Schema.Types.ObjectId,
    //     unique: true
    // }, 
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type:String
    },
    type: [{
        type: Schema.Types.ObjectId,
        ref: 'ProductType',
        required: true
    }],
    imageUrl:{
        type: String,
        required: true
    },
    buyPrice:{
        type: Number,
        required: true
    },
    promotionPrice:{
        type: Number,
        required: true
    },
    amount: {
    type: Number,
    default: 0
    },
    timeCreated: {
        type: Date,
        default: Date.now() 	
    },
    timeUpdated: {
        type: Date,
        default: Date.now() 	
    }
},{
    timestamps: true
})
module.exports = mongoose.model('product', productModel)