//Import thư viện mongoose
const mongoose = require('mongoose');

//khai báo class Schema;
const Schema = mongoose.Schema;

//khai báo ProductType
const customerModel = new Schema({
    // _id :{
    //     type: Schema.Types.ObjectId,
    //     unique: true
    // }, 
    fullName: {
        type: String,
        required: true
    },
    phone:{
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    address: {
        type: String,
        default: ""
    },
    city: {
        type: String,
        default: ""
    },
    country: {
        type: String,
        default: ""
    },
    orders: [{
        type:Schema.Types.ObjectId,
        ref: 'Order'
    }],
    timeCreated: {
        type: Date,
        default: Date.now() 	
    },
    timeUpdated: {
        type: Date,
        default: Date.now() 	
    }
},{
    timestamps: true
})
module.exports = mongoose.model('customer', customerModel)