//Import thư viện mongoose
const mongoose = require('mongoose');

//Import order model
const orderModel = require('../model/orderModel');

const getAllOders = (req, res) => {
// B1: Thu thập dữ liệu từ req
// B2: Validate dữ liệu
// B3: Gọi model thực hiện các thao tác nghiệp vụ
orderModel.find((error, data) => {
if(error) {
    return res.status(500).json({
        message: error.message
    })
}
return res.status(200).json({
    message: 'Get all Customer',
    customer: data
})
})
}
//Get productType by id
const getOrderById = (req, res) => {
// B1: Thu thập dữ liệu từ req
let orderID = req.params.orderID;
// B2: Validate dữ liệu
if(!mongoose.Types.ObjectId.isValid(orderID)){
    return res.status(400).json({
        message: 'Customer ID is invalid'
    })
}
// B3: Gọi model thực hiện các thao tác nghiệp vụ
orderModel.find(orderID, (error,data) => {
    if(error){
        return res.status(500).json({
        message:error.message
        })
    }
    return res.status(200).json({
        message:'Get a Customer by ID',
        customer: data
    })
})
}
//Create ProductType
const createOrder = (req, res) => {
// B1: Thu thập dữ liệu từ req
let body = req.body;
// B2: Validate dữ liệu
if(!body.orderDate){
    return res.status(400).json({
        message:'Order OrderDate is required!'
    })
}
// B3: Gọi model thực hiện các thao tác nghiệp vụ
let newOrder = {
_id: mongoose.Types.ObjectId,
orderDate: body.orderDate,
shippedDate: body.shippedDate,
note: body.note,
orderDetaill: body.orderDetaill,
cost: body.cost,
}
orderModel.create(newOrder, (error, data) => {
    if(error){
        return res.status(500).json({
            message:error.message
            })
    }
    return res.status(201).json({
        message:'Create successfully',
        order: data
    })
})
}
//Update ProductType
const updateOrder = (req, res) => {
// B1: Thu thập dữ liệu từ req
let orderID = req.params.orderID;
let body = req.body;

// B2: Validate dữ liệu
if(!mongoose.Types.ObjectId.isValid(orderID)){
    return res.status(400).json({
        message: 'Order ID is invalid'
    })
}
// B3: Gọi model thực hiện các thao tác nghiệp vụ
let orderUpdate = {
    _id: mongoose.Types.ObjectId,
    orderDate: body.orderDate,
    shippedDate: body.shippedDate,
    note: body.note,
    orderDetaill: body.orderDetaill,
    cost: body.cost,
}
orderModel.findByIdAndUpdate(orderID, orderUpdate, (error, data) => {
    if(error){
        return res.status(500).json({
            message:error.message
            })
    }
    return res.status(200).json({
        message:'Update successfully',
        order: data
    })
})
}
//delete productType 
const deleteOrder = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let orderID = req.params.orderID;
// B2: Validate dữ liệu
if(!mongoose.Types.ObjectId.isValid(orderID)){
    return res.status(400).json({
        message: 'Customer ID is invalid'
    })
}
// B3: Gọi model thực hiện các thao tác nghiệp vụ
orderModel.findOneAndDelete(orderID, (error, data)=>{
    if(error){
        return res.status(500).json({
            message:error.message
            })
    }
    return res.status(200).json({
        message:'Update successfully',
    })
})
}


module.exports = {
    getAllOders,
    getOrderById,
    createOrder,
    updateOrder,
    deleteOrder
}