//Import thư viện Express
const express = require('express');
const { productMiddleware } = require('../middlewares/productMiddleware');
const {  getAllProduct,
    getProductById,
    createProduct,
    updateProduct,
    deleteProduct
} = require('../controllers/productController');

//Tạo router
const productRouter = express.Router();

//Sử dụng middleware
productRouter.use(productMiddleware);

//Get all product
productRouter.get('/product', getAllProduct)

//Get product by Id
productRouter.get('/product/:productID',getProductById )

//Create a product
productRouter.post('/product', createProduct);

//Update product
productRouter.put('/product/:productID', updateProduct);
 
//Delete product
productRouter.delete('/product/:productID', deleteProduct);

module.exports = { productRouter };